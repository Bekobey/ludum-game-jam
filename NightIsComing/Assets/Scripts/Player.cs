﻿using System.Collections;
using UnityEngine.UI;
using UnityEngine;
using TMPro;

public class Player : MonoBehaviour
{
    [Header("Movement Stats")]
    public float speed;
    public int jumps;
    public float jumpForce;
    public float timeBetweenTrail;
    public float groundCheckRadius;

    [Header("Combat Stats")]
    public int health = 100;
    public int damage = 40;
    public float attackSpeed = 1.5f;
    public float comboMaxDelay = 1.5f;
    public float weaponDuration = .3f;
    public BoxCollider2D rightArm;
    public BoxCollider2D leftArm;
    public float attackForce = 50;
    public float attackJumpForce = 30;

    [Header("Player Conditions")]
    public bool isBlocking;
    public bool isAttacking;

    [Header("Prefabs")]
    public GameObject jumpParticle;
    public GameObject runParticle;
    public GameObject playerDeadParticles;

    public GameObject particleParentObject;
    public Slider HealthBar;
    public Transform groundCheck;
    public LayerMask whatIsGround;
    public string[] stepSounds;

    private float timeBetweenTrailValue;
    private float horizontalMove;
    private int remainingJumps;
    private float weaponEnableCooldown;
    private float attackCooldown;
    private float comboCooldown;
    private int comboCount;
    private bool facingRight = false;
    private bool onGround;
    private bool spawnJumpParticles;

    private Animator anim;
    private Rigidbody2D rb;
    private GameManager gm;

    void Start()
    {
        timeBetweenTrailValue = timeBetweenTrail;
        remainingJumps = jumps;
        rb = GetComponent<Rigidbody2D>();
        gm = GetComponentInParent<GameManager>();
        anim = GetComponentInChildren<Animator>();
    }

    private void Update()
    {
        if (gm.playerDead || gm.gameIsPaused)
            return;

        ManageMovement();
        SpawningRunParticles();
        Attack();
    }

    void FixedUpdate()
    {
        if (gm.playerDead || gm.gameIsPaused)
            return;

        ManageJumpingAndFallingAnim();
        FlipManager();
    }

    void ManageMovement()
    {
        onGround = Physics2D.OverlapCircle(groundCheck.position, groundCheckRadius, whatIsGround);

        if (isAttacking && onGround)
        {
            rb.velocity = Vector2.Lerp(rb.velocity, new Vector2(0, rb.velocity.y), .2f);
            return;
        }

        horizontalMove = Input.GetAxisRaw("Horizontal") * speed;
        rb.velocity = new Vector2(horizontalMove, rb.velocity.y);
        anim.SetFloat("Speed", Mathf.Abs(horizontalMove));

        if (onGround == true)
        {
            anim.SetBool("IsFalling", false);
            anim.SetBool("IsJumping", false);
            remainingJumps = jumps;
        }

        if (Input.GetKeyDown(KeyCode.W) && remainingJumps > 0)
        {
            GameObject jumpExplosion = Instantiate(jumpParticle, groundCheck.position, Quaternion.identity);
            jumpExplosion.transform.parent = particleParentObject.transform;
            Destroy(jumpExplosion, 1);
            rb.velocity = Vector2.up * jumpForce;
            remainingJumps--;
            SpawnParticle(jumpParticle);
        }

        /*
        else if (Input.GetKeyDown(KeyCode.W) && remainingJumps == 0 && onGround == true)
        {
            GameObject jumpExplosion = Instantiate(jumpParticle, groundCheck.position, Quaternion.identity);
            jumpExplosion.transform.parent = particleParentObject.transform;
            Destroy(jumpExplosion, 1);
            onGround = false;
            rb.velocity = Vector2.up * jumpForce;
        }
        */
    }

    void ManageJumpingAndFallingAnim()
    {
        if (rb.velocity.y > 0)
        {
            anim.SetBool("IsJumping", true);
        }

        else
        {
            anim.SetBool("IsJumping", false);
        }

        if (rb.velocity.y < -.1f)
        {
            anim.SetBool("IsFalling", true);
        }

        else
        {
            anim.SetBool("IsFalling", false);
        }
    }

    void Attack()
    {
        if (attackCooldown <= 0)
        {
            if (isAttacking)
            {
                isAttacking = false;
            }

            if (Input.GetButton("Fire1"))
            {
                if (comboCount >= 3 || comboCooldown <= 0)
                {
                    comboCount = 0;
                }

                comboCount++;
                comboCooldown = comboMaxDelay;
                Debug.Log("Attack " + comboCount);

                if (Mathf.Abs(rb.velocity.y) > 1.5f && !onGround)
                {
                    anim.SetTrigger("Attack 3");
                }

                else
                {
                    anim.SetTrigger("Attack " + comboCount);

                    if (comboCount > 1)
                    {
                        if (transform.localScale.x < 0)
                        {
                            rb.velocity = new Vector2(attackForce, 0);
                        }

                        else
                        {
                            rb.velocity = new Vector2(-attackForce, 0);
                        }
                    }
                }

                attackCooldown = 1 / attackSpeed;

                StartCoroutine(EnableWeapons());
                isAttacking = true;
            }
        }

        else
        {
            attackCooldown -= Time.deltaTime;
        }

        if (comboCooldown > 0)
        {
            comboCooldown -= Time.deltaTime;
        }
    }

    void SpawningRunParticles()
    {
        if (onGround && Mathf.Abs(rb.velocity.x) > 0 && !isAttacking)
        {
            if (timeBetweenTrail <= 0)
            {
                SpawnParticle(runParticle);
                timeBetweenTrail = timeBetweenTrailValue;
            }
            else
            {
                timeBetweenTrail -= Time.deltaTime;
            }
        }
    }

    void SpawnParticle(GameObject particle)
    {
        gm.am.Play(stepSounds[Random.Range(0,3)]);
        GameObject newParticle = Instantiate(particle, groundCheck.position, Quaternion.identity);
        newParticle.transform.parent = particleParentObject.transform;
        Destroy(newParticle, 1);
    }

    void FlipManager()
    {
        if (facingRight == false && horizontalMove > 0)
        {
            Flip();
        }

        else if (facingRight == true && horizontalMove < 0)
        {
            Flip();
        }
    }

    void Flip()
    {
        facingRight = !facingRight;
        Vector3 Scaler = gameObject.transform.localScale;
        Scaler.x *= -1;
        gameObject.transform.localScale = Scaler;
    }

    public void UpdateHealth(int damage)
    {
        health -= damage;
        HealthBar.value = health;
        if (HealthBar.value <= 0 && !gm.playerDead)
        {
            StartCoroutine(IfDeadLoseGame());
        }
    }

    IEnumerator IfDeadLoseGame()
    {
        gm.playerDead = true;
        anim.SetTrigger("Die");
        yield return new WaitForSecondsRealtime(0.8f);

        //gm.am.Play("playerHit");
        GameObject explosion = Instantiate(playerDeadParticles, transform.position, Quaternion.identity);
        gm.PlayerDied();
        gameObject.SetActive(false);
    }

    IEnumerator EnableWeapons()
    {
        rightArm.enabled = true;
        leftArm.enabled = true;
        yield return new WaitForSecondsRealtime(0.5f);

        /*
        if (transform.localScale.x < 0)
        {
            ApplyForce(Vector2.right,attackForce);
        }

        else
        {
            ApplyForce(Vector2.left,attackForce);
        }

        if (Mathf.Abs(rb.velocity.y) > 1.5f && !onGround)
        {
            ApplyForce(Vector2.up, attackJumpForce);
            Debug.Log("Attack Jump");
        }
        */
        yield return new WaitForSecondsRealtime(0.1f);
        rightArm.enabled = false;
        leftArm.enabled = false;
    }

    void ApplyForce(Vector2 direction, float force)
    {
        rb.AddForce(direction * force, ForceMode2D.Impulse);
    }
}
