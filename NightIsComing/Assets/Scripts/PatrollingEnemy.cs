﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PatrollingEnemy : MonoBehaviour
{
    public float patrollingSpeed;
    public float groundDetectionRaycastLength;
    public Transform groundDetection;

    private bool movingRight=false;
    private Vector2 movingDirection= Vector2.left;


    void Update()
    {
        transform.Translate(movingDirection * patrollingSpeed * Time.deltaTime);
        RaycastHit2D groundInfo = Physics2D.Raycast(groundDetection.position,Vector2.down, groundDetectionRaycastLength);

        if (groundInfo.collider == false)
        {
            {
                if (movingRight)
                {
                    Flip();
                }
                else
                {
                    Flip();
                }
            }
        }
    }

    void Flip()
    {
        movingRight = !movingRight;
        movingDirection = -1 * movingDirection;
        Vector3 Scaler = gameObject.transform.localScale;
        Scaler.x *= -1;
        gameObject.transform.localScale = Scaler;
    }
}
