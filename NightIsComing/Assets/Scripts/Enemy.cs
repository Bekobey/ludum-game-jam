﻿using UnityEngine;
using System.Collections;

public class Enemy : MonoBehaviour
{
    [Header("Combat Stats")]
    public int health;
    public int minDamage;
    public int maxDamage;
    public float maxHitDistance;
    public float attackSpeed;
    public bool canGetAngry;
    public bool isAngry;
    public float angerDistance;
    public bool isStunned;

    [Header("Movement Stats")]
    public float speed;
    public float jumpForce;
    public float minDistance;
    float direction = 1;

    [Header("Patrolling Stats")]
    public float patrollingSpeed;
    public float groundDetectionRaycastLength;
    public Transform groundDetection;

    private bool movingRight = false;
    private Vector2 movingDirection = Vector2.left;
    private Animator anim;

    GameObject player;
    Player playerScript;
    PatrollingEnemy patrollingEnemyScript;
    GameManager gm;

    float attackCooldown;

    void Start()
    {
        player = GameObject.FindWithTag("Player");
        playerScript = player.GetComponent<Player>();
        anim = GetComponentInChildren<Animator>();
    }

    void Update()
    {
        CheckAnger();

        if (isAngry)
        {
            if (isStunned)
                return;

            TryToAttack();
            MoveTowardsPlayer();
        }

        else
        {
            //Patroll();
        }
    }

    void Patroll()
    {
        transform.Translate(movingDirection * patrollingSpeed * Time.deltaTime);
        RaycastHit2D groundInfo = Physics2D.Raycast(groundDetection.position, Vector2.down, groundDetectionRaycastLength);

        if (groundInfo.collider == false)
        {
            if (movingRight)
            {
                Flip();
            }

            else
            {
                Flip();
            }
        }
    }

    void Flip()
    {
        movingRight = !movingRight;
        movingDirection = -movingDirection;
        direction = -direction;

        transform.localScale = new Vector3 (-transform.localScale.x, transform.localScale.y, 1);
    }

    void CheckAnger()
    {
        if (!canGetAngry)
            return;

        float distance = Vector2.Distance(transform.position, player.transform.position);

        if (distance < angerDistance)
        {
            isAngry = true;
        }
    }

    void MoveTowardsPlayer()
    {
        anim.SetBool("Walk", true);

        float distance = Vector2.Distance(transform.position, player.transform.position);

        if (distance > minDistance)
        {
            transform.position = Vector2.MoveTowards(transform.position, new Vector2(player.transform.position.x, transform.position.y), speed * Time.deltaTime);
        }

        if (player.transform.position.x > transform.position.x && direction > 0)
        {
            Flip();
        }

        else if (player.transform.position.x < transform.position.x && direction < 0)
        {
            Flip();
        }
    }

    void TryToAttack()
    {
        if (attackCooldown <= 0)
        {
            float distance = Vector2.Distance(transform.position, player.transform.position);

            if (distance < maxHitDistance)
            {
                HitPlayer();
            }
        }

        else
        {
            attackCooldown -= Time.deltaTime;
        }
    }

    void HitPlayer()
    {
        anim.SetTrigger("Attack");
        attackCooldown = 1 / attackSpeed;

        if (playerScript.isBlocking)
            return;

        int damage = Random.Range(minDamage, maxDamage);
        playerScript.UpdateHealth(damage);
    }

    void Die()
    {
        gameObject.SetActive(false);
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "Player Weapon")
        {
            health -= playerScript.damage;

            if (health <= 0)
            {
                Die();
            }
        }
    }

    IEnumerator Stun(float duration)
    {
        isStunned = true;
        yield return new WaitForSeconds(duration);
        isStunned = false;
    }
}
