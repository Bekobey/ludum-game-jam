﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;
using TMPro;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    public GameObject player;
    public GameObject EnemiesParentObject;
    public int totalEnemyCount;
    private int killedEnemies;

    public GameObject pauseMenu;
    public GameObject deathScreen;
    public GameObject transitionScreen;
    
    [HideInInspector] public bool gameIsPaused = false;
    [HideInInspector] public AudioManager am;
    [HideInInspector] public bool playerDead = false;

    void Start()
    {
        am = GetComponentInChildren<AudioManager>();
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            if (gameIsPaused)
            {
                Resume();
            }
            else
            {
                Pause();
            }
        }
    }

    public void EnemyKilled()
    {
        killedEnemies++;

        if (killedEnemies == totalEnemyCount)
        {
            Complete();
        }
    }

    public void Resume()
    {
        pauseMenu.SetActive(false);
        Time.timeScale = 1f;
        gameIsPaused = false;
    }

    void Pause()
    {
        pauseMenu.SetActive(true);
        Time.timeScale = 0f;
        gameIsPaused = true;
    }

    public void Restart()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }

    public void PlayerDied()
    {
        //StartCoroutine(EndingScene());
        deathScreen.SetActive(true);
    }

    public void Complete()
    {
        transitionScreen.SetActive(true);
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
    }

    IEnumerator DeathScreen()
    {
        yield return new WaitForSeconds(1f);
        transitionScreen.SetActive(true);
    }
}
